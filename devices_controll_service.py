import serial.tools.list_ports
from collections import namedtuple
import struct
from threading import Thread, Event, Lock
import zmq
import time
import socket as nativeSocket
import serial
import json
# import RPi.GPIO as GPIO
import sys
import os
from subprocess import Popen
import Adafruit_PCA9685
import datetime
import math
import Adafruit_ADS1x15
import pingparsing

debug = 2                        # show debug info 0-no debug, 1-critical, 2 verbose, 3-everything
context = zmq.Context()
drone_service_ip = "192.168.3.2"    # send data to this drone IP
drone_service_port = 7002           # send data to this drone PORT
local_service_port = 7001           # listen data from drone in this PORT
drone_antenna_tracker_ip = "127.0.0.1"       # send data to this antenna tracker IP
drone_antenna_tracker_port = 7011   # send data to this antenna tracker PORT
terminate_all = False               # terminate application
camera_angle = 0.0                  # camera pitch angle in deg
old_angle = 0.0                     # camera pitch angle being processed
reset_total_amps = False
fence_breach_msg = None             # msg from geofencing thread
network_state_msg = None            # msg from network state thread
#cells_actual_voltage = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
cycling = True
zmq_server_string = "tcp://192.168.11.1:5555"
response_list = []
pixhawk_state = {'last_response': int(time.time() * 1000),
                 'gps': {'long': 0, 'lat': 0, 'alt': 0},
                 'gps_timestamp': int(time.time() * 1000),
                 'gps_sats': {'gps_fix': 0, 'gps_num_sat': 0},
                 'gps_sats_timestamp': int(time.time() * 1000),
                 'state': 'disarmed',
                 'state_timestamp': int(time.time() * 1000),
                 'vehicle_mode': None,
                 'vehicle_mode_timestamp': int(time.time() * 1000),
                 'system_state': None,
                 'system_state_timestamp': int(time.time() * 1000),
                 'home_position': None,
                 'obey_rc_input': None,
                 'obey_rc_input_timestamp': int(time.time() * 1000)
                 }

station_state = {'last_response': int(time.time() * 1000),
                 'gps':
                     {'long': 0, 'lat': 0}
                 }
safe_cp_lock = Lock()               # for locking response while sending
safe_drone_lock = Lock()               # for locking drone commands while sending
drone_commands = []             # drone commands to send to pix api
e_start_now = Event()           # information event to start sending drone commands immediately
e_start_now_camera_Master = Event()           # information event to start rotating main camera
#e_start_now_camera_Slave = Event()           # information event to start rotating slave camera

voltage_p_cell = {0: 0, 1: 0, 2: 0, 3: 0, 4: 0, 5: 0, 6: 0, 7: 0, 'V': 0}
current = {'total': 0, 'current': 0}    # total , current draw in Amps
system_state = {'high': {  #  high priority data - MUST BE OK
                    'drone': 0,  # 3 - no connection to pix controller, 0 - all good
                    'threads': {
                        'update_antenna_tracker': 0,
                        'send_drone_commands': 0,
                        'watch_connection': 0,
                        'voltage_data_read': 0,
                        'gather_pixhawk_data': 0,
                        'start_video_transmission': 0,
                        'current_data_read': 0,
                        'network_state_daemon': 0,
                        'geo_fencing': 0,
                        'camera_controll_Main': 0
                    }},
                'low': {  # low priority data
                    "station": 0  # 3 or 2 - no connection to Ground station for 20s or 2s, 0 - all good
                    }
                }
transmit = False

temp_sensor_time = [0.0, 0.0, 0.0]
temp_sensor_temperature = [30.1, 30.1, 30.1]

drone_variables = {
                    'ttyID': 'USB VID:PID=067B:2303 LOCATION=1-1.3',   # ttyID battery usb interface
                    'pcm': {1: 2, 2: 3, 3: 5, 4: 4, 5: 7, 6: 6, 7: 0, 8: 1},  # pin_id->cell_id mapping
                    'voltage_p_point': {0: 3.82 / 793, 1: 7.64 / 790, 2: 11.47 / 765, 3: 15.3 / 777, 4: 19.14 / 666,
                                       5: 22.97 / 685, 6: 26.8 / 778, 7: 30.63 / 693},
                    'cell_count': 8,
                    'cell_min_v': -100,
                    'dummy_voltage': 0,      #get dummy voltage readings bypassing voltage sensor
                    'acs758': 'b',  # acs758 u- unidirectional, b- bidirectional
                    'ttyCameraIDMain': 'USB VID:PID=10C4:EA60 SER=0001 LOCATION=1-1.2',
                    'maxCameraPitch': 90.0,
                    'minCameraPitch': -30.0,
                    'cameraOffset': -30,
                    'camFrameRollPitch': True,  #Fixed (to drone) axis roll, pitch axis is float
                    'reverseCameraPitch': False,
                    'camPort':'5001',
                    'camIP': '192.168.100.241',
                    'fenceAlt': 40,
                    'fenceRad': 5,
                    'allow_bat_ah': 0.1     # battery allowed limit in mAh, after witch drone goes into RTL mode. 0 - ignore
                                            # good idea to make 90% of your max (real) battery capacity
                   }
                # dummy_voltage : minutes -> set 3.7 for minutes time, then reduce to 3.6v. IF minutes 0, then disable dummy voltage
                    

def getserial():
    # Extract serial from cpuinfo file
    cpuserial = "0000000000000000"
    try:
        f = open('/proc/cpuinfo', 'r')
        for line in f:
            if line[0:6] == 'Serial':
                cpuserial = line[10:26]
        f.close()
    except:
        cpuserial = "ERROR000000000"

    return cpuserial


def getDroneVariables():
    global drone_variables
    cpu_serial = getserial()
    if cpu_serial == "0000000012e71a9f":  #main (octo) drone unique ID
        drone_variables['ttyID'] = '3f201000.serial'
        drone_variables['ttyCameraIDMain'] = 'USB VID:PID=10C4:EA60 SER=0001 LOCATION=1-1.1.3'
        drone_variables['reverseCameraPitch'] = True
        drone_variables['pcm'] = {1: 0, 2: 1, 3: 2, 4: 3, 5: 4, 6: 5, 7: 6, 8: 7}
        # drone_variables['voltage_p_point'] = {0: (5.0 / 1023), 1: 8.21 / 859, 2: 12.31 / 825, 3: 16.42 / 838,
        #                                       4: 20.4 / 729, 5: 24.5 / 738, 6: 28.6 / 748, 7: 32.7 / 754}
        drone_variables['voltage_p_point'] = {0: (4.88 / 1023), 1: 9.78 / 1023, 2: 15.25 / 1023, 3: 20.03 / 1023,
                                              4: 28.74 / 1023, 5: 34.00 / 1023, 6: 39.13 / 1023, 7: 32.7 / 754}  #5 - 24.8
        drone_variables['dummy_voltage'] = 10
        drone_variables['camFrameRollPitch'] = False
        drone_variables['maxCameraPitch'] = 30
        drone_variables['minCameraPitch'] = -90
        drone_variables['cameraOffset'] = -30
        
    elif cpu_serial == "0000000049eca6c3":  #test (hex) drone unique ID
        drone_variables['ttyID'] = 'USB VID:PID=10C4:EA60 SER=0001 LOCATION=1-1.3'  #'USB VID:PID=067B:2303 LOCATION=1-1.3'
        drone_variables['pcm'] = {1: 2, 2: 3, 3: 5, 4: 4, 5: 7, 6: 6, 7: 0, 8: 1}
        drone_variables['voltage_p_point'] = {0: 3.82 / 793, 1: 7.64 / 790, 2: 11.47 / 765, 3: 15.3 / 777, 4: 19.14 / 666,
                            5: 22.97 / 685, 6: 26.8 / 778, 7: 30.63 / 693}
        drone_variables['acs758'] = 'u'
        drone_variables['camPort'] = '5002'
        drone_variables['ttyCameraIDMain'] = 'USB VID:PID=10C4:EA60 SER=0001 LOCATION=1-1.2'
        drone_variables['maxCameraPitch'] = 30
        drone_variables['minCameraPitch'] = -70
        drone_variables['dummy_voltage'] = 0
        drone_variables['allow_bat_ah'] = 3.0  # left - 4020 ;right - 3820
        drone_variables['cameraOffset'] = 0


def lg(log_message, level):
    global response_list
    if level <= debug:
        print (datetime.datetime.now()),
        if level == 1:
            print("[CRITICAL ]"),
            response_list.append({'error':{'rpi_sensors':log_message}})
        if level == 2:
            print ("[IMPORTANT]"),
        if level == 3:
            print("[VERBOSE  ]"),
        if isinstance(log_message, list):
            print (">>"),
            for message in log_message:
                print ("<>"),
                try:
                    print (message),
                except:
                    print ('none asci message')
            print ("<<")
        else:
            try:
                print(log_message)
            except:
                print('none asci message')


# os.chdir(sys.argv[1])
# print('a')
# cat_pid = Popen(["(cat bufferout | nc 192.168.1.233 5001)"], shell=True)
# print('a')
# a_pid = Popen(["./a.out"], shell=True)
# print('a')
# rpivid = Popen(["raspivid -t 0 -w 1296 -h 972 -fps 49 -b 5000000 -o bufferin"], shell=True)
# time.sleep(1)
# print(cat_pid)
# print(a_pid)
# print(rpivid)
# time.sleep(10)
# rpivid.kill()
# time.sleep(2)
#
# exit(0)


def start_video_transmission():
    this_t = 'start_video_transmission'
    global transmit
    try:
        os.chdir(sys.argv[1])  # go to video scripts directory
    except:
        lg([this_t, "Please pass video scripts directory_name", sys.argv[1]], 1)
        # exit(0)
    while not terminate_all:
        system_state['high']['threads']['start_video_transmission'] = int(time.time() * 1000)
        if transmit:
            lg([this_t, "starting video transmission 1/2"], 2)
            a_pid = Popen(["./a.out", drone_variables['camIP'], drone_variables['camPort']])
            time.sleep(0.5)  # wait for connection
            a_pid_code = a_pid.poll()  # get process status
            if a_pid_code is None:  # checking if connection is made and no error code received
                lg([this_t, "starting video transmission 2/2"], 2)
                # rpivid = Popen(["/usr/bin/raspivid", "-t", "0", "-w", "1296", "-h", "972",
                #                 "-fps", "49", "-b", "5000000", "-o", "bufferin"])
                rpivid = Popen(["/usr/bin/raspivid", "-t", "0", "-w", "960", "-h", "960",
                                "-fps", "42", "-b", "20000000", "-sh", "50", "-o", "bufferin"])
                # "-fps", "42", "-b", "20000000", "-sh", "50", "-o", "bufferin"])
                a_pid.wait()

                rpivid.terminate()

            else:
                lg([this_t, "start video transmission terminated", a_pid_code], 1)

            transmit = False
        time.sleep(1)

###################### START ######################
######## camera controll movements  ###############
###################################################

CMD_CONTROL = 67
SBGC_CONTROL_MODE_NO = 0
SBGC_CONTROL_MODE_SPEED = 1
SBGC_CONTROL_MODE_ANGLE = 2
SBGC_CONTROL_MODE_SPEED_ANGLE = 3
SBGC_CONTROL_MODE_RC = 4
SBGC_CONTROL_MODE_ANGLE_REL_FRAME = 5
SBGC_SPEED_SCALE = float(1.0 / 0.1220740379)
SBGC_ANGLE_FULL_TURN = 16384
SBGC_DEGREE_ANGLE_SCALE = float(SBGC_ANGLE_FULL_TURN / 360.0)

ControlData = namedtuple(
    'ControlData',
    'controll_mode roll_speed roll_angle pitch_speed pitch_angle yaw_speed yaw_angle')

Message = namedtuple(
    'Message',
    'start_character command_id payload_size header_checksum payload payload_checksum')


def pack_control_data(control_data):
    return struct.pack('<Bhhhhhh', *control_data)


def create_message(command_id, payload):
    payload_size = len(payload)
    return Message(start_character=ord('>'),command_id=command_id,
                   payload_size=payload_size,
                   header_checksum=(command_id + payload_size) % 256,
                   payload=payload,
                   payload_checksum=(sum(bytearray(payload)) % 256))


def pack_message(message):
    message_format = '<BBBB{}sB'.format(message.payload_size)
    return struct.pack(message_format, *message)


def SBGC_DEGREE_TO_ANGLE(degree):
    return degree * SBGC_DEGREE_ANGLE_SCALE


def camera_angle_controll_Main(e_start_now_camera_Master):
    this_t = 'camera_angle_controll_Main'

    if drone_variables['ttyCameraIDMain'] is None:
        print('camera Main serial is not set')
        return

    ttycamera=None

    for port in serial.tools.list_ports.comports():
        if drone_variables['ttyCameraIDMain'] == port.hwid:
            ttycamera = port.device

    if ttycamera is None:
        print('No camera Main serial device found')
        return

    global old_angle
    connection = serial.Serial(ttycamera, baudrate=115200, timeout=10)
    old_angle = camera_angle

    while not terminate_all:
        system_state['high']['threads']['camera_controll_Main'] = int(time.time() * 1000)
        e_start_now_camera_Master.wait(1)
        e_start_now_camera_Master.clear()
        #if old_angle != camera_angle:
        if True:
            old_angle = camera_angle

            if drone_variables['camFrameRollPitch']:
                control_data = ControlData(controll_mode=SBGC_CONTROL_MODE_ANGLE,
                                           roll_speed=100 * SBGC_SPEED_SCALE, roll_angle=SBGC_DEGREE_TO_ANGLE(0),
                                           pitch_speed=100 * SBGC_SPEED_SCALE,pitch_angle=SBGC_DEGREE_TO_ANGLE(old_angle),
                                           yaw_speed=100 * SBGC_SPEED_SCALE, yaw_angle=0)
            else:
                control_data = ControlData(controll_mode=SBGC_CONTROL_MODE_ANGLE,
                                           roll_speed=100 * SBGC_SPEED_SCALE, roll_angle=SBGC_DEGREE_TO_ANGLE(old_angle),
                                           pitch_speed=100 * SBGC_SPEED_SCALE, pitch_angle=SBGC_DEGREE_TO_ANGLE(0),
                                           yaw_speed=100 * SBGC_SPEED_SCALE, yaw_angle=0)

            packed_control_data = pack_control_data(control_data)
            message_data = create_message(CMD_CONTROL, packed_control_data)
            packed_message = pack_message(message_data)
            connection.write(packed_message)
            lg([this_t, 'writing to camera Main'], 3)
        time.sleep(1)


# def camera_angle_controll_Slave(e_start_now_camera_Slave):    # deprecated
#     system_state['high']['threads']['camera_controll_Slave'] = int(time.time() * 1000)
#     if drone_variables['ttyCameraIDSlave'] is None:
#         print('camera Slave serial is not set')
#         return
#
#     ttycamera = None
#
#     for port in serial.tools.list_ports.comports():
#         if drone_variables['ttyCameraIDSlave'] == port.hwid:
#             ttycamera = port.device
#
#     if ttycamera is None:
#         print('No camera Slave serial device found')
#         return
#     connection = serial.Serial(ttycamera, baudrate=115200, timeout=10)
#
#     while not terminate_all:
#         e_start_now_camera_Slave.wait()
#         e_start_now_camera_Slave.clear()
#         control_data = ControlData(controll_mode=SBGC_CONTROL_MODE_ANGLE,
#                                    roll_speed=100 * SBGC_SPEED_SCALE, roll_angle=SBGC_DEGREE_TO_ANGLE(0+7),
#                                    pitch_speed=100 * SBGC_SPEED_SCALE, pitch_angle=SBGC_DEGREE_TO_ANGLE(old_angle),
#                                    yaw_speed=100 * SBGC_SPEED_SCALE, yaw_angle=0)
#
#         packed_control_data = pack_control_data(control_data)
#         message_data = create_message(CMD_CONTROL, packed_control_data)
#         packed_message = pack_message(message_data)
#         connection.write(packed_message)
#         lg('writing to camera Slave', 2)

###################################################
######## camera controll movements  ###############
###################   END   #######################


def update_antenna_tracker():
    this_t = 'update_antenna_tracker'
    lg([this_t, 'DRONE ANTENNA TRACKER NOT FIXED/IMPLEMENTED'],1)
    """ Sends data to drone antenna tracker for movements and direction """

    global drone_commands
    if not (drone_antenna_tracker_ip and drone_antenna_tracker_port):
        lg([this_t, "drone antenna tracker ip or port not set"], 1)
        return

    antenna_sock = nativeSocket.socket(nativeSocket.AF_INET, nativeSocket.SOCK_DGRAM)
    last_pix_gps = {}
    gps_to_send = {'gps': ''}

    while not terminate_all:
        if last_pix_gps != pixhawk_state['gps']:
            last_pix_gps = pixhawk_state['gps']
            lg([this_t, "new gps coordinates, sending gps of drone, and station"], 3)

            gps_to_send['gps'] = last_pix_gps
            gps_to_send['gps']['device'] = 'drone'
            antenna_sock.sendto(json.dumps(gps_to_send), (drone_antenna_tracker_ip, drone_antenna_tracker_port))
            lg([this_t, gps_to_send], 3)

            gps_to_send['gps'] = station_state['gps']
            gps_to_send['gps']['device'] = "station"
            antenna_sock.sendto(json.dumps(gps_to_send), (drone_antenna_tracker_ip, drone_antenna_tracker_port))
            lg([this_t, gps_to_send], 3)
            time.sleep(0.2)
        else:
            lg([this_t, "requesting new coordinates"], 3)
            drone_commands.append('get_drone_gps')
            time.sleep(1)
        system_state['high']['threads']['update_antenna_tracker'] = int(time.time() * 1000)
    antenna_sock.close()

# def watch_temp(threadName, port_nr):
#     global temp_sensor_time
#     global temp_sensor_temperature
#     UDP_IP = "192.168.3.2"
#     UDP_PORT = 1111+port_nr
#
#     sock = socket.socket(socket.AF_INET, # Internet
#                      socket.SOCK_DGRAM) # UDP
#     sock.bind((UDP_IP, UDP_PORT))
#
#     while True:
#         data, addr = sock.recvfrom(1024) # buffer size is 1024 bytes
#         temp_sensor_time[port_nr] = int(time.time() * 1000)
#         temp_sensor_temperature[port_nr] = float(data)

# def temp_controller(threadName, data):
#     global temp_sensor_time
#     global temp_sensor_temperature
#     GPIO.setmode(GPIO.BCM)
#     GPIO.setup(18, GPIO.OUT)
#     GPIO.output(18, GPIO.LOW)
#     heat_up = False
#     max_temp = 30
#     cur_max_temp = 0
#
#     UDP_IP = "192.168.3.1"
#     UDP_PORT = 1100
#
#     sock = socket.socket(socket.AF_INET, # Internet
#                      socket.SOCK_DGRAM) # UDP
#     while True:
#         for x in range(0, 3):
# 	    if (int(time.time() * 1000) - temp_sensor_time[x] < 3000): # if sensor is still operating
#                 if max_temp < temp_sensor_temperature[x]: #if true temp higher then threshold
#                     heat_up = False
#                 #else:
#                 if temp_sensor_temperature[x] > cur_max_temp :
#                     cur_max_temp = temp_sensor_temperature[x]
#     		    print cur_max_temp, temp_sensor_temperature[x]
#             else:
#                 heat_up = False # sensor is down! fuck!!!
# 		print 'problem'+str(temp_sensor_temperature[x])+' '+str(x)
# 	    print '| '+str(temp_sensor_temperature[x])+' '+str(x)+' |'
#
# 	print heat_up
#         if heat_up:
# 	    print 'heating'
#             GPIO.output(18, GPIO.HIGH)
#         else:
#             GPIO.output(18, GPIO.LOW)
# 	    print 'cooling'
#         heat_up = True
#         sock.sendto(str(cur_max_temp), (UDP_IP, UDP_PORT))
#         cur_max_temp = 0
#         time.sleep(1)


def set_tune(enable):
    if enable:
        duty = (((1800 - 1000) / 100) + 2.0) / 100
        lg("PWM set : tune enable duty: " + str(duty), 2)
    else:
        duty = (((1400 - 1000) / 100) + 2.0) / 100
        lg("PWM set : tune disabled duty: " + str(duty), 2)
    pwm.set_pwm(7, 0, int(4096.0 * duty))


def set_althold(enable):
    if enable:
        duty = (((1800 - 1000) / 100) + 2.0) / 100
        lg("PWM set : althold enable duty: " + str(duty), 2)
    else:
        duty = (((1400 - 1000) / 100) + 2.0) / 100
        lg("PWM set : stabilyze enabled duty: " + str(duty), 2)
    pwm.set_pwm(6, 0, int(4096.0 * duty))


def set_poshold(enable):
    if enable:
        duty = (((1600 - 1000) / 100) + 2.0) / 100
        lg("PWM set : position hold enable duty: " + str(duty), 2)
        pwm.set_pwm(6, 0, int(4096.0 * duty))


def set_camera_pitch_angle(pwm_data):  # rc_1234 c45 1266 1899 ...
    this_t = 'set_camera_pitch_angle'
    global camera_angle
    rc_channels = pwm_data.split(' ')
    rc_channels.remove('rc_1234')
    lg([this_t, rc_channels], 3)
    for rc_channel in rc_channels:
        if rc_channel.startswith('c'):  # if roll -> 1
            try:
                camera_angle = float(rc_channel[1:])
            except:
                lg([this_t, 'Wrong camera angle!!!:', rc_channel[1:]], 1)
                camera_angle = 0
            if drone_variables['reverseCameraPitch']:
                camera_angle = camera_angle * -1
            if camera_angle > drone_variables['maxCameraPitch']:
                camera_angle = drone_variables['maxCameraPitch']
            if camera_angle < drone_variables['minCameraPitch']:
                camera_angle = drone_variables['minCameraPitch']
            camera_angle += drone_variables['cameraOffset']
            lg([this_t, "camera_angle " + str(camera_angle)], 2)
            e_start_now_camera_Master.set()


def network_state_daemon():
    this_t = 'network_state_daemon'
    global network_state_msg
    
    ping_parser = pingparsing.PingParsing()
    transmitter = pingparsing.PingTransmitter()
    transmitter.destination = "192.168.11.1"
    transmitter.count = 5
    transmitter.packet_size = 1400
    transmitter.timeout = 1
    transmitter.deadline = 10
    transmitter.ping_option = "-l 5"

    while not terminate_all:
        system_state['high']['threads']['network_state_daemon'] = int(time.time() * 1000)
        result = transmitter.ping()
        ping_stats = ping_parser.parse(result).as_dict()

        # "rtt_avg": 2.075,
        # "packet_transmit": 5,
        # "packet_duplicate_rate": 0.0,
        # "rtt_max": 2.357,
        # "packet_duplicate_count": 0,
        # "packet_loss_count": 0,
        # "packet_loss_rate": 0.0,
        # "rtt_min": 1.808,
        # "destination": "192.168.11.1",
        # "packet_receive": 5,
        # "rtt_mdev": 0.229

        if ping_stats['packet_loss_count'] or ping_stats['rtt_avg'] > 1000.0:
            network_state_msg = "communication link unstable, loosing " + str(ping_stats['packet_loss_count']) + \
                                " packets out of " + str(ping_stats['packet_transmit']) + ", avg delay " + \
                                str(ping_stats['rtt_avg']) + "ms."
            lg([this_t, network_state_msg], 1)
        else:
            lg([this_t, ping_stats],3)
            network_state_msg = None
        for s in range(5):
            system_state['high']['threads']['network_state_daemon'] = int(time.time() * 1000)
            time.sleep(1)


def system_error_state_handler():  # shows light based on programs state 1blink/s - internals work ok, 2b/s full ok, else - error
    this_t = 'system_error_state_handler'

    lg([this_t, 'starting system_error_state_handler process in 3s'], 3)
    time.sleep(3)
    global fence_breach_msg
    global network_state_msg

    # pixhawk_state = {'last_response': int(time.time() * 1000),
    #                  'gps': {'long': 0, 'lat': 0, 'alt': 0},
    #                  'gps_timestamp': int(time.time() * 1000),
    #                  'gps_sats': {'gps_fix': 0, 'gps_num_sat': 0},
    #                  'gps_sats_timestamp': int(time.time() * 1000),
    #                  'state': 'disarmed',
    #                  'state_timestamp': int(time.time() * 1000),
    #                  'vehicle_mode': None,
    #                  'vehicle_mode_timestamp': int(time.time() * 1000),
    #                  'system_state': None,
    #                  'system_state_timestamp': int(time.time() * 1000),
    #                  'home_position': None
    #                  }
    #
    # station_state = {'last_response': int(time.time() * 1000),
    #                  'gps':
    #                      {'long': 0, 'lat': 0}
    #                  }
    # voltage_p_cell = {0: 0, 1: 0, 2: 0, 3: 0, 4: 0, 5: 0, 6: 0, 7: 0, 'V': 0}
    # current = {'total': 0, 'current': 0}  # total , current draw in Amps

    # GPIO.setmode(GPIO.BCM)
    # GPIO.setwarnings(False)
    # GPIO.setup(26, GPIO.OUT)
    # GPIO.output(26, GPIO.LOW)
    command_send_time = int(time.time() * 1000) - 20000

    while not terminate_all:
        system_state_severity = 0   # 0 good (do nothing); 1 fixable (return home pos); 2 severe (TRL); 3 critical (land NOW!)
        current_time = int(time.time() * 1000)
        
        if current_time - system_state['high']['threads']['start_video_transmission'] > 2000 and not transmit:
            system_state_severity = 0
            lg([this_t,"thread start_video_transmission is dead"], 1)

        if current_time - system_state['high']['threads']['camera_controll_Main'] > 2000:
            system_state_severity = 1
            lg([this_t,"thread Main camera controll is dead"], 1)

        if system_state['high']['drone'] == 3 and current_time - system_state['high']['threads']['watch_connection'] > 2000:
            system_state_severity = 1
            lg([this_t,"Drone is armed but not responding"], 1)  # its severe problems, but there is nothing we can do :(

        if current_time - system_state['high']['threads']['update_antenna_tracker'] > 2000:
            system_state_severity = 1
            lg([this_t,"thread update_antenna_tracker is dead"], 1)

        if current_time - system_state['high']['threads']['network_state_daemon'] > 2000:
            system_state_severity = 1
            lg([this_t, "thread network_state_daemon is dead"], 1)
        else:
            if network_state_msg is not None and \
                    pixhawk_state["vehicle_mode"] not in ['rtl', 'land'] and \
                    pixhawk_state['system_state'] not in ["standby", None]:  # if network glitching is registered
                system_state_severity = 1
                lg([this_t,network_state_msg], 1)
            
        if current_time - system_state['high']['threads']['geo_fencing'] > 2000:
            system_state_severity = 1
            lg([this_t,"thread geo_fencing is dead"], 1)
        else:
            if fence_breach_msg is not None and \
                    pixhawk_state["vehicle_mode"] not in ['rtl', 'land'] and \
                    pixhawk_state['system_state'] not in ["standby", None]:  # if fence breach is registered
                system_state_severity = 1
                lg([this_t,fence_breach_msg], 1)

        if current_time - system_state['high']['threads']['send_drone_commands'] > 2000:
            system_state_severity = 2
            lg([this_t,"thread send_drone_commands is dead"], 1)

        if current_time - system_state['high']['threads']['gather_pixhawk_data'] > 2000:
            system_state_severity = 2
            lg([this_t,"thread gather_pixhawk_data is dead"], 1)

        if current_time - system_state['high']['threads']['voltage_data_read'] > 2000:
            system_state_severity = 3
            lg([this_t,"thread voltage_data_read is dead"], 1)
        else:
            for cell_id in range(drone_variables['cell_count']):
                if voltage_p_cell[cell_id] < drone_variables['cell_min_v']:
                    system_state_severity = 3
                    lg([this_t, 
                        "cell "+str(cell_id)+"->"+str(voltage_p_cell[cell_id])+' below '+str(drone_variables['cell_min_v'])], 1)

        if current_time - system_state['high']['threads']['current_data_read'] > 2000:
            system_state_severity = 3
            lg([this_t,"thread current_data_read is dead"], 1)
        else:
            if drone_variables['allow_bat_ah'] < current['total']:
                system_state_severity = 3
                lg([this_t, "used batt ah "+str(current['total'])+" > " + str(drone_variables['allow_bat_ah']) ], 1)

        if current_time - system_state['high']['threads']['watch_connection'] > 2000:
            system_state_severity = 3
            lg([this_t,"thread watch_connection is dead"], 1)
        # if watch_connection thread is working, check comunication between station
        elif system_state['low']['station'] == 3:
            system_state_severity = 3

        # lg(['system_error_state_handler','current_time - command_send_time                    ',current_time - command_send_time],1)
        # lg(['system_error_state_handler','pixhawk_state[system_state] not in ["standby", None]', pixhawk_state['system_state'] not in ["standby", None]], 1)
        # lg(['system_error_state_handler','current_time - pixhawk_state[system_state_timestamp]', current_time - pixhawk_state['system_state_timestamp']], 1)
        
        
        # got system state severity. take action if :
        # 1. we executed action long time ago (>20000ms) 
        # 2. and drone is airborn
        # 3. pixhawk is responding (does this last statement make any sense?)
        if system_state_severity \
                and current_time - command_send_time > 20000 \
                and pixhawk_state['system_state'] not in ["standby", None] \
                and current_time - pixhawk_state['system_state_timestamp'] < 2000:

            if system_state_severity == 1 and pixhawk_state["vehicle_mode"] not in ['rtl', 'land']:  # if we already doing severity 2 commands
                lg([this_t, "system severity fixable(1)"], 3)
                drone_commands.append('go_home_position')
            if system_state_severity == 2 and pixhawk_state["vehicle_mode"] not in ['rtl', 'land']:  # if we already doing severity 2 commands
                lg([this_t, "system severity severe(2)"], 2)
                drone_commands.append('rtl')
            if system_state_severity == 3 and pixhawk_state["vehicle_mode"] not in ['land']:  # if we already doing severity 3 cmd
                lg([this_t, "system severity critical(3)"], 1)
                drone_commands.append('land')
            command_send_time = current_time


        # if blink == 1:
        #     for x in range(0, 1):
        #         GPIO.output(26, GPIO.HIGH)
        #         time.sleep(0.5)
        #         GPIO.output(26, GPIO.LOW)
        #         time.sleep(0.5)
        # if blink == 2:
        #     for x in range(0, 3):
        #         GPIO.output(26, GPIO.HIGH)
        #         time.sleep(0.25)
        #         GPIO.output(26, GPIO.LOW)
        #         time.sleep(0.25)
        # if not blink:
        #     GPIO.output(26, GPIO.HIGH)
        #     time.sleep(0.2)
        #     GPIO.output(26, GPIO.LOW)
        #     time.sleep(1.8)
        time.sleep(2)
    # GPIO.cleanup(26)
    time.sleep(1)
    lg([this_t, "THREAD done"], 2)


def watch_connection():
    this_t = 'watch_connection'
    global terminate_all
    global response_list
    system_state['high']['drone'] = 0

    while True:
        if terminate_all:
            # send info to drone about termination of this service
            response_list.append({'client_state': 'shutting_down'})
            lg([this_t, "THREAD watch_connection done "], 2)
            return

        if pixhawk_state['state'] == 'armed':
            delta_time = int(int(time.time() * 1000) - pixhawk_state['last_response'])
            if delta_time > 2000:
                system_state['high']['drone'] = 3
                lg([this_t, "connection to drone lost for more than 2s !!!"], 1)
            else:
                system_state['high']['drone'] = 0


        delta_time = int(int(time.time() * 1000) - station_state['last_response'])

        if (pixhawk_state['system_state'] in ["standby", None]) or delta_time < 2000:
            system_state['low']['station'] = 0  # ok
        elif delta_time > 20000:
            system_state['low']['station'] = 3  # severe
            lg([this_t, "connection to station lost for more than 20s !!!"], 1)
        else:
            lg([this_t, "connection to station lost for more than 2s !!!"], 1)
            system_state['low']['station'] = 1  #sort a problem, but dont give a f*ck

        time.sleep(0.5)
        system_state['high']['threads']['watch_connection'] = int(time.time() * 1000)


def send_drone_commands(e_start_now):
    this_t = 'send_drone_commands'
    global terminate_all
    global drone_commands
    global response_list
    global pixhawk_state

    lg([this_t, "setting up socket to pixhawk"], 2)

    # set up data socket to be send to pixhawk api
    sock_send = nativeSocket.socket(nativeSocket.AF_INET, nativeSocket.SOCK_DGRAM)

    do_iterations = 10  # do iterarions before exiting thread

    while do_iterations or not terminate_all:
        time_now = int(time.time() * 1000)
        while drone_commands:
            #  if we got vehicle data long time ago (2s) request that data
            if time_now - pixhawk_state['vehicle_mode_timestamp'] > 2000:
                sock_send.sendto(json.dumps({'msg': 'get_state'}), (drone_service_ip, drone_service_port))
                # update timestamp not to request again before response
                pixhawk_state['vehicle_mode_timestamp'] = time_now
                lg([this_t, "MSG -> Drone: " + 'get_state'], 2)
            try:
                safe_drone_lock.acquire()
                drone_exec = drone_commands[0]
                drone_commands.remove(drone_exec)
            finally:
                safe_drone_lock.release()

            lg([this_t, "new drone command request " + drone_exec], 3)
            if drone_exec in ['9', 'arm', 'disarm', 'vid', 'take_off', 'land', 'stab', 'loiter', 'get_state',
                              'get_altitude', 'get_home_position', 'get_rc_ch', 'set_rc_ch', 'velocity', 'rtl',
                              'guided', 'yaw', 'poshold', 'go_home_position', 'man', 'dig']:
                sock_send.sendto(json.dumps({'msg': drone_exec}), (drone_service_ip, drone_service_port))
                lg([this_t, "MSG -> Drone: " + drone_exec], 2)

            if drone_exec == 'get_drone_gps':
                sock_send.sendto(json.dumps({'msg': 'get_position'}), (drone_service_ip, drone_service_port))
                lg([this_t, "MSG -> Drone: get_position"], 2)

            if drone_exec.startswith('rc_1234'):
                sock_send.sendto(json.dumps({'msg': drone_exec}), (drone_service_ip, drone_service_port))
                lg([this_t, "MSG -> Drone: " + drone_exec], 2)

        if terminate_all:
            do_iterations = do_iterations-1
        lg([this_t, "no new drone command request"], 3)
        e_start_now.wait(0.5)  # sleep for .5s or until we get event command
        e_start_now.clear()  # clearing event for new listenning
        system_state['high']['threads']['send_drone_commands'] = time_now
    sock_send.close()
    lg([this_t, "THREAD send_drone_commands done"], 2)


# reply all data to Statio. All data gathered in response list
# and ready to be sent to station
def reply_all_data():  # this one replies all data to station
    this_t = 'reply_all_data'
    global response_list
    global pixhawk_state
    # always reply voltage and amperage
    if len(response_list) > 100:
        del response_list[:]
    response_list.append({'voltage': voltage_p_cell})  #appending critical data
    response_list.append({'current': current})  # appending critical data
    response_list.append({'pixhawk_state': pixhawk_state})

    lg([this_t, "replaying", response_list], 3)

    try:
        safe_cp_lock.acquire()
        response_list_send = list(response_list)
        del response_list[:]
    finally:
        safe_cp_lock.release()

    lg([this_t, "MSG -> station: ", response_list_send], 2)

    socket.send_json(json.dumps(response_list_send))
    lg([this_t, "all data replied to ZMQ server"], 3)


# geofence calculations on loiter, poshold, stabilize, althold modes
def geo_fencing():
    this_t = 'geo_fencing'
    global system_state
    global fence_breach_msg
    while not terminate_all:
        system_state['high']['threads']['geo_fencing'] = int(time.time() * 1000)

        if pixhawk_state['home_position'] is not None:  # request home location data if not set
            # check if all variables are set and drone in control (loiter, poshold) mode
            if pixhawk_state['gps']['long'] != 0 and pixhawk_state['vehicle_mode'] in ['loiter', 'poshold', 'stab', 'althold']:
                lg([this_t,'Calculating fence breach', pixhawk_state['home_position'], pixhawk_state['gps']], 3)
                # lg(pixhawk_state['home_position'], 3)
                # lg(pixhawk_state['gps'], 3)

                ext_lat = math.radians(pixhawk_state['gps']['lat'])
                ext_lon = math.radians(pixhawk_state['gps']['long'])
                int_lat = math.radians(pixhawk_state['home_position']['lat'])
                int_lon = math.radians(pixhawk_state['home_position']['long'])

                dlon = int_lon - ext_lon
                dlat = int_lat - ext_lat

                p1 = dlon * math.cos((int_lat + ext_lat) / 2)
                p2 = dlat
                distance = 6371000 * math.sqrt(p1 * p1 + p2 * p2)  # distance in meter
                fence_breach_msg = None
                lg([this_t,'fence distance: ' + str(distance)], 3)

                if distance > drone_variables['fenceRad']:
                    fence_breach_msg = 'Fence breach!!! too FAR: ' + str(distance)+" > " + str(drone_variables['fenceRad'])
                    # lg('Fence breach!!! too FAR: ' + str(distance)+" > " + str(drone_variables['fenceRad']), 1)
                elif pixhawk_state['gps']['alt'] < 2.0:
                    fence_breach_msg = 'Fence breach!!! too LOW: ' + str(pixhawk_state['gps']['alt']) + " < 2m"
                elif pixhawk_state['gps']['alt'] > drone_variables['fenceAlt']:
                    fence_breach_msg = 'Fence breach!!! too HIGH: ' + str(pixhawk_state['gps']['alt']) + " > " + str(
                        drone_variables['fenceAlt'])
            else:
                fence_breach_msg = None

        else:
            lg([this_t,'No Home position set, requesting Home position ...'], 3)
            drone_commands.append('get_home_position')
        time.sleep(1)

    time.sleep(1)


def gather_pixhawk_data():
    """
    gather response from pixhawk client api and populate pixhawk_state variable.
    Data (response_list and pixhawk_state) is sent on reply_all_data(), initiated from main cycle. 
    """
    this_t = 'gather_pixhawk_data'
    global pixhawk_state
    global response_list
    # set up data socket to get pixhawk responses
    sock_get = nativeSocket.socket(nativeSocket.AF_INET, nativeSocket.SOCK_DGRAM)
    sock_get.settimeout(10.0)
    sock_get.bind(("", local_service_port))
    system_state['high']['threads']['gather_pixhawk_data'] = int(time.time() * 1000)
    lg([this_t, "waiting for pixhawk data"], 2)

    while not terminate_all:

        got_pix_invalid_msg = True
        try:
            pix_message, sensors_ip = sock_get.recvfrom(1024)
        except:
            lg([this_t, 'time out listening on pixhawk port, retrying'], 1)
            pix_message = {}

        lg([this_t, pix_message], 3)
        try:
            json_message = json.loads(pix_message)
        except:
            lg([this_t, pix_message], 1)
            json_message = {}

        system_state['high']['threads']['gather_pixhawk_data'] = int(time.time() * 1000)

        if json_message != {}:
            pixhawk_state['last_response'] = system_state['high']['threads']['gather_pixhawk_data']
            lg([this_t, "MSG <- Drone :", json_message], 2)
            # lg(json_message, 2)

        if 'arm_msg' in json_message:
            if json_message['arm_msg'] in ['armed', 'disarmed']:
                pixhawk_state['state_timestamp'] = pixhawk_state['last_response']
                pixhawk_state['state'] = json_message['arm_msg']
                response_list.append(json_message)
                got_pix_invalid_msg = False
            elif json_message['arm_msg'] == 'error':  # in case of error lets find out state of the machine
                pixhawk_state['state_timestamp'] = pixhawk_state['last_response']
                lg([this_t, 'arm_msg == error', json_message['info']], 1)
                # lg(json_message['info'], 1)
                if json_message['info'] == 'could not disarm':
                    pixhawk_state['state'] = 'armed'
                else:
                    pixhawk_state['state'] = 'disarmed'
                json_message['arm_msg'] = json_message['arm_msg']+" : "+json_message['info']
                response_list.append(json_message)  # inform main server
                got_pix_invalid_msg = False

        if 'obey_rc_input' in json_message:
            pixhawk_state['obey_rc_input'] = json_message['obey_rc_input']
            pixhawk_state['obey_rc_input_timestamp'] = pixhawk_state['last_response'];
            got_pix_invalid_msg = False

        if 'gps' in json_message:
            pixhawk_state['gps'] = json_message['gps']
            pixhawk_state['gps_timestamp'] = pixhawk_state['last_response'];
            got_pix_invalid_msg = False

        if 'gps_fix' in json_message:
            pixhawk_state['gps_sats']['gps_fix'] = json_message['gps_fix']
            pixhawk_state['gps_sats_timestamp'] = pixhawk_state['last_response'];
            got_pix_invalid_msg = False

        if 'gps_num_sat' in json_message:
            pixhawk_state['gps_sats']['gps_num_sat'] = json_message['gps_num_sat']
            pixhawk_state['gps_num_sat_timestamp'] = pixhawk_state['last_response'];
            got_pix_invalid_msg = False

        if 'system_state' in json_message:
            pixhawk_state['system_state'] = json_message['system_state']
            pixhawk_state['system_state_timestamp'] = pixhawk_state['last_response'];
            got_pix_invalid_msg = False

        if 'vehicle_mode' in json_message:
            pixhawk_state['vehicle_mode'] = json_message['vehicle_mode']
            pixhawk_state['vehicle_mode_timestamp'] = pixhawk_state['last_response'];
            got_pix_invalid_msg = False

        if 'home_position' in json_message:
            pixhawk_state['home_position'] = json_message['home_position']
            got_pix_invalid_msg = False

        if 'doing_state' in json_message or 'altitude' in json_message or 'client_state' in json_message:
            response_list.append(json_message)
            got_pix_invalid_msg = False

        if 'rc_channels' in json_message or 'error' in json_message:
            response_list.append(json_message)
            got_pix_invalid_msg = False

        if got_pix_invalid_msg:
            lg([this_t, 'unknown data received from pixhawk raw:', pix_message, "json:", json_message], 1)
            # lg(pix_message, 1)
            # lg(json_message, 1)
            # lg('unknown data received from pixhawk end (look above)', 1)

    sock_get.close()
    lg([this_t, "THREAD gather_pixhawk_data done"], 2)


def current_data_read():
    this_t = 'current_data_read'
    global current
    global reset_total_amps
    file_path = '../last_amperage_data'
    adc = Adafruit_ADS1x15.ADS1115()
    GAIN = 2/3
    adc.start_adc(0, gain=GAIN)

    if drone_variables['acs758'] == 'u':
        adc_offset = 0  # adc offset 0v = -3
        acs758_offset = 3199  # 0.600 # ACS offset .6V
        scale_factor = 0.04  # mV per Amp
        V1 = 6.144  # adc voltage calculated on
        N1 = 32767  # adc raw voltage reading on V1
        apb = (V1 / N1) / scale_factor  # Amp per Bit
    else:
        # device specs: 32767 - 6.144
        scale_factor = 0.018  # 20mV per Amp
        adc_offset = 0
        V1 = 0.0002  # V1 = 6.144  # max adc voltage calculated on V1
        N1 = 1  # N1 = 32767  # max adc raw voltage reading on V1
        acs758_offset = 13730  # ACS offset 2.5737035432 V - 13726
        apb = (V1 / N1) / scale_factor  # Amp per Bit

        #  10.39A/825 = 0.01259393939A/acs_digit
        # 0.00020/0.016 = 0.0125  = apb
        # apb = 0.0110

    total_amps = 0

    try:
        f = open(file_path, "r")
        total_amps_saved = float(f.read())
        f.close()
    except IOError:
        total_amps_saved = 0
    except ValueError:
        total_amps_saved = 0

    lg([this_t, "THREAD current_data_read started "], 2)
    lg([this_t, "total_amps "+str(total_amps_saved)], 2)
    while not terminate_all:
        samples_count = 0
        samples_sum = 0
        start = time.time()
        system_state['high']['threads']['current_data_read'] = int(start * 1000)
        while (time.time() - start) <= 1.0:
            value = adc.get_last_result() + adc_offset
            #print 'adc value --> ' + str(value)
            time.sleep(0.01)
            samples_sum += value
            samples_count += 1

        current_amps = ((samples_sum / samples_count) - acs758_offset) * apb
        total_amps += (current_amps / 3600.0)
        current = {'total': (total_amps + total_amps_saved), 'current': current_amps}

        if reset_total_amps:
            total_amps = 0.0
            total_amps_saved = 0.0
            reset_total_amps = False
        lg([this_t, 'amperage :'+str(current_amps)+' total ah :'+str(current['total'])], 3)

    f = open(file_path, "w+")
    f.write(str(current['total']))
    f.close()

    adc.stop_adc()


    lg([this_t, "THREAD current_data_read done"], 2)


# start drone dummy voltage readings for dummy_voltage minutes, then sets cell voltages to low(3.6volts)
def voltage_data_read_dummy():
    global voltage_p_cell
    start_time = time.time()
    set_low_after_sec = drone_variables['dummy_voltage'] * 60
    voltage_p_cell = {0: 4.2, 1: 4.0, 2: 3.9, 3: 3.8, 4: 3.7, 5: 3.7, 6: 3.7, 7: 3.7, 'V': 32.2}
    while not terminate_all:
        system_state['high']['threads']['voltage_data_read'] = int(time.time() * 1000)
        time.sleep(1)
        end_time = time.time()
        # lg(['time elapsed in s:',end_time - start_time,'time threshold in s',set_low_after_sec], 1)
        if (end_time - start_time) > set_low_after_sec:
            voltage_p_cell = {0: 3.8, 1: 3.7, 2: 3.6, 3: 3.6, 4: 3.6, 5: 3.6, 6: 3.6, 7: 3.6, 'V': 28.8}


def voltage_data_read():
    """
    # cell  - pin   - 5.13v - voltage/point mappting
    # c0    - pin7  - 1024	- 3.82 = 793
    # c1    - pin8  - 528   - ...
    # c2    - pin1  - 340   - ...
    # c3    - pin2  - 258   - ...
    # c4    - pin4  - 176   - ...
    # c5    - pin3  - 151   - ...
    # c6    - pin6  - 146   - ...
    # c7    - pin5  - 113   - ...
    :return:
    """
    # pcm - pin cell mapping
    # pcm = {0: 0, 1: 1, 2: 2, 3: 3, 4: 4, 5: 5, 6: 6, 7: 7} [pin_id]->[cell_id]
    # if drone_cell_pin_mapping == "pix":
    #     pcm = {1: 2, 2: 3, 3: 5, 4: 4, 5: 7, 6: 6, 7: 0, 8: 1}
    # else:
    #     if drone_cell_pin_mapping == "":
    #         lg("THREAD voltage_data_read disabled ", 2)
    #         return

    # voltage_p_point = {0:3.82 / 793, 1:7.64 / 790, 2:11.47 / 765, 3: 15.3 / 777, 4: 19.14 / 666, 5: 22.97 / 685, 6: 26.8 / 778, 7: 30.63 / 693}

    this_t = 'voltage_data_read'
    pcm = drone_variables['pcm']

    lg([this_t, "THREAD voltage_data_read started "], 2)

    global voltage_p_cell
    analog_outputs = {0: 0, 1: 0, 2: 0, 3: 0, 4: 0, 5: 0, 6: 0, 7: 0}
    voltage_p_point = drone_variables['voltage_p_point']
    true_voltage = {0: 0, 1: 0, 2: 0, 3: 0, 4: 0, 5: 0, 6: 0, 7: 0}

    if drone_variables['ttyID'] is None:
        lg([this_t, "Battery serial is not set"], 1)
        return

    ttybattery = None

    for port in serial.tools.list_ports.comports():
        if drone_variables['ttyID'] == port.hwid:
            ttybattery = port.device

    if ttybattery is None:
        lg([this_t, 'No Battery serial device found'], 1)
        return

    port = serial.Serial(ttybattery, baudrate=4800, timeout=3.0)

    while not terminate_all:
        data = port.readline().split(":")
        try:  # checking if data is readable
            data[0] = int(data[0])
            lg([this_t, 'voltage_data_read', data[0], int(data[1])], 3)
        except:
            data[0] = 10  # make dummy data

        if 1 <= data[0] <= 8:
            if True:
                lg([this_t, "cell :" + str(pcm[data[0]])], 3)
                analog_outputs[pcm[data[0]]] = int(data[1])  # wite analog data by cell to analog_outputs array
                lg([this_t, "analog_outputs :"+str(analog_outputs[pcm[data[0]]])], 3)
                true_voltage[pcm[data[0]]] = analog_outputs[pcm[data[0]]] * voltage_p_point[pcm[data[0]]]  #convert analog data to voltage
                lg([this_t, 'voltage_data_read',"true_voltage :" + str(true_voltage[pcm[data[0]]])], 3)
                if pcm[data[0]] > 0:  # if cell is not first cell
                    #  recalculate individual voltage by cell
                    voltage_p_cell[pcm[data[0]]] = round(true_voltage[pcm[data[0]]] - true_voltage[(pcm[data[0]]-1)], 3)
                    if (drone_variables['cell_count'] - 1) == pcm[data[0]]:  # if this is the last cell - get whole voltage of a battery
                        voltage_p_cell['V'] = round(true_voltage[pcm[data[0]]], 3)
                else:
                    voltage_p_cell[pcm[data[0]]] = round(true_voltage[pcm[data[0]]], 3)

                lg([this_t, "voltage_p_cell :" + str(voltage_p_cell[pcm[data[0]]])], 3)
        system_state['high']['threads']['voltage_data_read'] = int(time.time() * 1000)

    port.close()
    lg([this_t, "THREAD voltage_data_read done"], 2)


#try:
#    t_temp_udp0 = Thread(target=watch_temp, args=("watch_temp", 0,))
#    t_temp_udp0.start()
#except:
#    print "Error: unable to get temperature sensors connection 0"

#try:
#    t_temp_udp1 = Thread(target=watch_temp, args=("watch_temp", 1,))
#    t_temp_udp1.start()
#except:
#    print "Error: unable to get temperature sensors connection 1"

#try:
#    t_temp_udp2 = Thread(target=watch_temp, args=("watch_temp", 2,))
#    t_temp_udp2.start()
#except:
#    print "Error: unable to get temperature sensors connection 2"

#try:
#    t_temp_controller = Thread(target=temp_controller, args=("watch_temp", 2,))
#    t_temp_controller.start()
#except:
#    print "Error: unable to get temperature_controller thread"

#pitch_angle = motor("m2", camera_PITCH, simulation=False)
#if debug:
#    print'starting camera pulses ..', camera_PITCH

#pitch_angle.start()
#pitch_angle.setW(50)
#time.sleep(1)

for port in serial.tools.list_ports.comports():
    print  "available USB ports hwid:"+ port.hwid

getDroneVariables()

# GPIO.setmode(GPIO.BCM)
# GPIO.setwarnings(False)

# set up camera pwm
pwm = Adafruit_PCA9685.PCA9685(0x40)
pwm.set_pwm_freq(50)

set_althold(False)
set_tune(False)

#set_camera_pitch_angle("rc_1234 c1800")  # 1800 -down
#set_camera_pitch_angle("rc_1234 c1600")  # 1600 - middle
#set_camera_pitch_angle("rc_1234 c1300")  # 1400 - up

lg("Connecting to ZMQ server", 2)
socket = context.socket(zmq.REP)
socket.connect(zmq_server_string)

# start thread to watch if connection between drone and station is ok
try:
    t_ping_pong = Thread(target=watch_connection)
    t_ping_pong.start()
except:
    lg("Error: unable to start connection watch thread",1)

try:
    t_drone_cmds = Thread(target=send_drone_commands, args=(e_start_now,))
    t_drone_cmds.start()
except:
    lg("Error: unable to start drone commands thread",1)


try:
    t_gps_broker = Thread(target=update_antenna_tracker)
    t_gps_broker.start()
except:
    lg("Error: unable to start drone update_antenna_tracker thread",1)

try:
    if drone_variables['dummy_voltage'] > 0:
        t_voltage = Thread(target=voltage_data_read_dummy)
    else:
        t_voltage = Thread(target=voltage_data_read)
    t_voltage.start()
except:
    lg("Error: unable to start voltage thread", 1)

try:
    t_current = Thread(target=current_data_read)
    t_current.start()
except:
    lg("Error: unable to start voltage thread", 1)


try:
    t_pixhawk_receive = Thread(target=gather_pixhawk_data)
    t_pixhawk_receive.start()
except:
    lg("Error: unable to start voltage thread", 1)

# start thread to sen data to indicator led
# try:
#     t_state = Thread(target=state_indicator)
#     t_state.start()
# except:
#     lg("Error: unable to start state_indicator thread", 1)

try:
    t_start_video_transmission = Thread(target=start_video_transmission)
    t_start_video_transmission.start()
except:
    lg("Error: unable to start start_video_transmission", 1)

# try:  # deprecated
#     t_start_camera_controll_Slave = Thread(target=camera_angle_controll_Slave, args=(e_start_now_camera_Slave,))
#     t_start_camera_controll_Slave.start()
# except:
#     lg("Error: unable to start camera_angle_controll_Slave", 1)

try:
    t_start_camera_controll_Main = Thread(target=camera_angle_controll_Main, args=(e_start_now_camera_Master,))
    t_start_camera_controll_Main.start()
except:
    lg("Error: unable to start t_start_camera_controll_Main", 1)

try:
    t_geo_fencing = Thread(target=geo_fencing)
    t_geo_fencing.start()
except:
    lg("Error: unable to start geo_fencing", 1)
    
try:
    t_network_state_daemon = Thread(target=network_state_daemon)
    t_network_state_daemon.start()
except:
    lg("Error: unable to start network_state_daemon", 1)

try:
    t_system_error_state_handler = Thread(target=system_error_state_handler)
    t_system_error_state_handler.start()
except:
    lg("Error: unable to start system_error_state_handler, terminating application",1)
    cycling = False
    time.sleep(2)

this_t = 'MAIN_cycling'
while cycling:
    #  if request is unknown, reply with message
    lg([this_t, "cycling"], 2)
    got_invalid_msg = True

    server_message = socket.recv_json()
    lg([this_t, "MSG <- station: "+str(server_message)], 2)

    message = json.loads(server_message)

    station_state['last_response'] = int(time.time() * 1000)

    # these commands are not in hight priority to execute
    if message['msg'] in ['arm', 'take_off', 'get_rc_ch', 'get_state']:
        lg([this_t, "got "+message['msg']], 3)
        got_invalid_msg = False
        drone_commands.append(message['msg'])

    # these commands are in hight priority to execute
    if message['msg'] in ['disarm', 'land', 'stab', 'loiter', 'set_rc_ch', 'velocity', 'yaw', 'rtl', 'guided', 'poshold','man','dig']:
        lg([this_t, "got " + message['msg']], 3)
        got_invalid_msg = False
        drone_commands.append(message['msg'])
        e_start_now.set()

    if message['msg'] == 'get_drone_gps':
        lg([this_t, "got " + message['msg']], 3)
        got_invalid_msg = False
        response_list.append(pixhawk_state['gps'])

    if (message['msg']).startswith('rc_1234'):
        lg([this_t, "got " + message['msg']], 3)
        got_invalid_msg = False
        drone_commands.append(message['msg'])
        e_start_now.set()
        set_camera_pitch_angle(message['msg'])

    if (message['msg']).startswith('set_station_gps'):
        lg([this_t, "got " + message['msg']], 3)
        got_invalid_msg = False

    if message['msg'] == '9':
        lg([this_t, "got " + message['msg']], 2)
        drone_commands.append(message['msg'])
        cycling = False
        got_invalid_msg = False
        lg([this_t, '====VERBOSE==IMPORTANT=========== got 9 msg, quiting application ========VERBOSE==IMPORTANT===='], 1)

    if message['msg'] == 'reset':
        lg([this_t, "got " + message['msg']], 2)
        reset_total_amps = True
        got_invalid_msg = False

    if message['msg'] == 'ping':
        lg([this_t, "got " + message['msg']], 3)
        response_list.append({'heartbeat': 'pong'})
        got_invalid_msg = False

# START set autotune freq on/off
    if message['msg'] == 't':
        got_invalid_msg = False
        set_tune(True)
    if message['msg'] == 'f':
        got_invalid_msg = False
        set_tune(False)
# END set autotune freq on/off
# START set stabilize/althold liter
    if message['msg'] == 'h':
        got_invalid_msg = False
        set_althold(True)
    if message['msg'] == 's':
        got_invalid_msg = False
        set_althold(False)
    if message['msg'] == 'p':
        got_invalid_msg = False
        set_poshold(True)
# END set stabilize/althold

    if message['msg'] == 'vid':
        lg([this_t, "got " + message['msg']], 3)
        transmit = True
        drone_commands.append(message['msg'])
        got_invalid_msg = False

    if got_invalid_msg:
        lg([this_t, "got unrecognizable msg " + message['msg']], 1)
        response_list.append({'msg': 'not recognized'})

    reply_all_data()

# pitch_angle.stop()
time.sleep(1)
terminate_all = True
time.sleep(3)
lg([this_t, "THREAD MAIN done "], 2)
