void setup() {
  // put your setup code here, to run once:
  Serial.begin(4800);
//  int ledPin = 13;  
//  int voltages[8];
}

void loop() {
  // put your main code here, to run repeatedly:
  int ledPin = 13;
  int voltages[8]={0,0,0,0,0,0,0,0};
  digitalWrite(ledPin, LOW);

  for ( int a = 0; a < 10; a++ ) {
    voltages[0] += analogRead(A0);
    delay(10);
    voltages[1] += analogRead(A1);
    delay(10);
    voltages[2] += analogRead(A2);
    delay(10);
    voltages[3] += analogRead(A3);
    delay(10);
    voltages[4] += analogRead(A4);
    delay(10);
    voltages[5] += analogRead(A5);
    delay(10);
    voltages[6] += analogRead(A6);
    delay(10);
    voltages[7] += analogRead(A7);
    delay(10);
  }
  for (int i=0; i< 8; i++){
    Serial.print((i+1));
    Serial.print(':');
    Serial.println((voltages[i])/10);
  }
  digitalWrite(ledPin, HIGH);
  delay(20);
}