#!/bin/bash

while true; do
    today=`date '+%Y_%m_%d__%H_%M_%S'`;
    script_path="/home/pi/2017_summer_drone/rpi_sensors"
    /usr/bin/python ${script_path}/devices_controll_service.py ${script_path}/x264_stuff > /tmp/devices_controll_service_log$today 2>&1 ;
    sleep 2;
done